'use strict';

angular.module('teamchatApp')
  .controller('AdminCtrl', function($scope, $http, Auth, User, Data) {
    $scope.users = User.query();
    $scope.messages = Data.query();
    $scope.isCleaned = false;

    $scope.deleteUser = function(user) {
      User.remove({
        id: user._id
      });
      angular.forEach($scope.users, function(u, i) {
        if (u === user) {
          $scope.users.splice(i, 1);
        }
      });
    };

    $scope.deleteMessage = function(message) {
      Data.remove({
        id: message._id
      });
      angular.forEach($scope.messages, function(m, i) {
        if (m === message) {
          $scope.messages.splice(i, 1);
        }
      });
    };

    $scope.clean = function() {
      Data.clean().$promise.then(function() {
        $scope.messages = Data.query();
        $scope.isCleaned = true;
      });
    };
  });
