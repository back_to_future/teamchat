'use strict';

angular.module('teamchatApp')
  .factory('Data', function($http, $q, $resource) {
    return $resource('/api/messages/:action/:offset/:id', {
      offset: '@_offset',
      id: '@_id',
    }, {
      getHistory: {
        method: 'GET',
        params: {
          action: 'getHistory'
        },
        isArray: true
      },
      getEarliedHistory: {
        method: 'POST',
        params: {
          action: 'getEarliedHistory'
        },
        isArray: true
      },
      clean: {
        method: 'POST',
        params: {
          action: 'clean'
        }
      },
      addMessage: {
        method: 'POST',
        transformRequest: function(data) {
          return JSON.stringify(data);
        },
        params: {
          action: 'addMessage'
        }
      }
    });
  }).filter('fromNow', function() {
    return function(date) {
      return moment(date).fromNow();
    };
  }).filter('to_trusted', ['$sce', function($sce) {
    return function(text) {
      var result = '';
      try {
        result = $sce.trustAsHtml(text);
      } catch (e) {
        result = text;
      }
      return result;
    };
  }]).factory('logger', function() {
    return function(message, level) {
      var currentDate = new Date();
      var log = '[' + currentDate + ']: ';
      switch (level) {
        case 'info':
          console.info(log, message);
          break;
        case 'warning':
          console.warn(log, message);
          break;
        case 'error':
          console.error(log, message);
          break;
        case 'log':
          console.log(log, message);
          break;
        default:
          console.log(log, message);
      }
    };
  });
