'use strict';

angular.module('teamchatApp')
  .controller('MainCtrl', function($scope, Data, socket, Auth, focus, User, logger) {
    $scope.usersOnline = [];
    $scope.messages = [];
    $scope.currentOffset = 2;
    $scope.isScrollToBottom = true;
    $scope.specific = false;
    $scope.message = '';

    $scope.currentUser = User.get();
    var checkOnlineInterval = 100000; // 1 min 40 sec
    var confirmOnlineInterval = 20000; // 30 sec

    // notifications
    var isTitleChangeable = false;
    var unreadMessages = 0;
    var currentTitle = document.title = 'Teamchat';

    var focusMessageBar = function() {
      var barId = 'message';
      focus(barId);
    };

    // Get online users
    var getOnlineUsers = function() {
      var users = User.onlineUsers();
      users.$promise.then(function(data) {
        $scope.usersOnline = [];
        data.forEach(function(user) {
          $scope.addUser(user.name);
        });
      });
    };

    var setOnline = function() {
      var status = User.setOnline();
      status.$promise.then(function(data) {
        if (data.status && data.status === 'success') {
          getOnlineUsers();
        }
      });
    };

    var notifications = function() {
      if (isTitleChangeable) {
        document.title = ' (' + unreadMessages + ') ' + currentTitle;
      } else {
        document.title = currentTitle;
      }
    };

    // User gone offline...
    window.onbeforeunload = function() {
      User.setOnline();
    };

    // User blur browser tab
    window.onblur = function() {
      isTitleChangeable = true;
    };

    // User focus browser tab
    window.onfocus = function() {
      isTitleChangeable = false;
      unreadMessages = 0;
      notifications();
      getOnlineUsers();
    };

    $scope.reply = function(username) {
      if (username === $scope.currentUser.name) {
        return;
      }
      var tag = '@';
      if ($scope.message === '' || $scope.message) {
        $scope.message = tag + username + ', ';
      } else {
        $scope.message += ' ' + tag + username + ' ';
      }
      focusMessageBar();
    };


    // Check if username exist in global userlist
    var checkUser = function(candidate) {
      for (var i = 0; i < $scope.usersOnline.length; i++) {
        var user = $scope.usersOnline[i].user;
        if (candidate === user) {
          return false;
        }
      }
      return true;
    };

    // Add a new message
    $scope.addMessage = function() {
      if ($scope.message !== '') {
        Data.addMessage({
          message: $scope.message,
          notforall: $scope.specific
        });
        $scope.message = '';
      }
      focusMessageBar();
    };

    // Add new user to online userlist
    $scope.addUser = function(username) {
      if (checkUser(username)) {
        $scope.usersOnline.push({
          user: username,
          current: $scope.currentUser.name === username,
        });
        logger('User ' + username + ' added to online list. Current online : ' + $scope.usersOnline.length, 'info');
      }
    };

    // Remove user from userlist
    $scope.removeUser = function(username) {
      var i = -1;
      if (!checkUser(username)) {
        for (i in $scope.usersOnline) {
          if (username === $scope.usersOnline[i].user) {
            $scope.usersOnline.splice(i, 1);
            logger('User ' + username + ' removed from online list. Current online : ' + $scope.usersOnline.length, 'info');
          }
        }
      }
    };

    $scope.getEarliedHistory = function() {
      $scope.isScrollToBottom = false;
      var messages = Data.getEarliedHistory({
        offset: $scope.currentOffset
      });
      messages.$promise.then(function(data) {
        // clear previos messages
        $scope.messages = [];
        data.forEach(function(message) {
          $scope.messages.push(message);
        });
        $scope.messages.reverse();
        $scope.currentOffset++;
      });
    };

    // First upload users status
    setOnline();
    focusMessageBar();
    // Set the interval to 1 min 30 sec for check user status
    setInterval(function() {
      setOnline();
    }, confirmOnlineInterval);

    /**
     * Call Data service's post method
     * Pull last messages
     * @return {void}
     */
    var history = Data.getHistory();
    history.$promise.then(function(data) {
      data.forEach(function(message) {
        $scope.messages.push(message);
      });
      $scope.messages.reverse();
    });

    /**
     * Socket receiver that listen message:save events
     * @return {void}
     */
    socket.socket.on('message:save', function(message) {
      logger({
        message: 'Socket message:save called',
        data: message
      });
      $scope.messages.push(message);
      if (isTitleChangeable) {
        unreadMessages++;
        notifications();
      }
    });

    /**
     * Socket receiver that listen user:online events
     * @return {void}
     */
    socket.socket.on('user:online', function(user) {
      logger('Socket user:online called. User: ' + user.name);
      $scope.addUser(user.name);
    });

    /**
     * Socket receiver that listen user:offline events
     * @return {void}
     */
    socket.socket.on('user:offline', function(user) {
      logger('Socket user:offline called. User: ' + user.name);
      $scope.removeUser(user.name);
    });
  });
