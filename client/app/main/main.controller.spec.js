'use strict';

describe('Controller: MainCtrl ->', function() {

  // load the controller's module
  beforeEach(module('teamchatApp'));
  beforeEach(module('socketMock'));

  var MainCtrl,
    scope,
    $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function(_$httpBackend_, $controller, $rootScope) {
    $httpBackend = _$httpBackend_;

    $httpBackend.expectGET('/api/users/me')
      .respond({
        '_id': '557fe0a8c2d82c1e3e99887c',
        'provider': 'local',
        'name': 'Admin',
        'email': 'admin@admin.com',
        '__v': 0,
        'isOnline': 1,
        'lastOnline': '2015-06-16T08:40:03.192Z',
        'role': 'admin'
      });
    $httpBackend.expectPOST('/api/users/online')
      .respond({
        'status': 'success'
      });

    $httpBackend.expectGET('/api/messages/getHistory')
      .respond([{
        '_id': '557fff392e9a02c96c1467d1',
        'content': 'specific test',
        '_user': {
          '_id': '557ffb2f2e9a02c96c1467cd',
          'name': 'Admin'
        },
        '__v': 0,
        'notforall': true,
        'date': '2015-06-16T10:49:29.750Z'
      }, {
        '_id': '557fff302e9a02c96c1467d0',
        'content': 'test3',
        '_user': {
          '_id': '557ffb2f2e9a02c96c1467cd',
          'name': 'Admin'
        },
        '__v': 0,
        'notforall': false,
        'date': '2015-06-16T10:49:20.394Z'
      }, {
        '_id': '557fff2e2e9a02c96c1467cf',
        'content': 'test2',
        '_user': {
          '_id': '557ffb2f2e9a02c96c1467cd',
          'name': 'Admin'
        },
        '__v': 0,
        'notforall': false,
        'date': '2015-06-16T10:49:18.247Z'
      }, {
        '_id': '557fff2b2e9a02c96c1467ce',
        'content': 'test1',
        '_user': {
          '_id': '557ffb2f2e9a02c96c1467cd',
          'name': 'Admin'
        },
        '__v': 0,
        'notforall': false,
        'date': '2015-06-16T10:49:15.700Z'
      }]);

    $httpBackend.expectGET('/api/users/onlineUsers')
      .respond([{
        '_id': '557fe0a8c2d82c1e3e99887c',
        'name': 'Admin'
      }]);


    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should be an admin as current user', function() {
    $httpBackend.flush();
    expect(scope.currentUser.name).toBe('Admin');
    expect(scope.currentUser.email).toBe('admin@admin.com');
    expect(scope.currentUser.isOnline).toBe(1);
    expect(scope.currentUser.role).toBe('admin');
  });

  it('should be couple of messages', function() {
    $httpBackend.flush();
    expect(scope.messages.length).toBe(4);
    expect(scope.messages[0]).toEqual(jasmine.objectContaining({
      _id: '557fff392e9a02c96c1467d1',
    }));
    expect(scope.messages[0]).toContain('content');
    expect(scope.messages[0]).toContain('_user');
    expect(scope.messages[0]).toContain('notforall');
    expect(scope.messages[0]).toContain('date');
    expect(scope.messages[0]._user).toContain('_id');
    expect(scope.messages[0]._user).toContain('name');
  });

  it('should be an admin online only', function() {
    $httpBackend.flush();
    expect(scope.usersOnline.length).toBe(1);
    expect(scope.usersOnline[0].user).toBe('Admin');
    expect(scope.usersOnline[0].current).toBe(true);
  });

  it('should be success when user set as online', function() {
    $httpBackend.flush();
    expect(scope.usersOnline.length).toBe(1);
    expect(scope.usersOnline[0].user).toBe('Admin');
    expect(scope.usersOnline[0].current).toBe(true);
  });

  it('should be a teamchat as current title', function() {
    expect(MainCtrl.currentTitle).toBe('Teamchat');
  });

  it('should be a one user online', function() {
    scope.addUser('test');
    expect(scope.usersOnline.length).toBe(1);
  });

  it('should be a unique user online', function() {
    scope.addUser('test');
    scope.addUser('test');
    expect(scope.usersOnline.length).toBe(1);
  });

  it('should be a unique users online', function() {
    scope.addUser('test');
    scope.addUser('test');
    scope.addUser('test1');
    scope.addUser('test2');
    expect(scope.usersOnline.length).toBe(3);
  });

});
