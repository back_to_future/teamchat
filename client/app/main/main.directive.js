'use strict';

angular.module('teamchatApp')
    .directive('scrollBottom', function() {
        return {
            scope: {
                scrollBottom: '='
            },
            link: function(scope, element) {
                scope.$watchCollection('scrollBottom', function() {
                    $(element).stop().animate({
                        scrollTop: $(element)[0].scrollHeight
                    }, 800);
                });
            }
        };
    }).directive('enterSubmit', function() {
        return {
            link: function(scope, elem, attrs) {
                elem.bind('keydown', function(event) {
                    var code = event.keyCode || event.which;
                    if (code === 13) {
                        if (!event.shiftKey) {
                            event.preventDefault();
                            scope.$apply(attrs.enterSubmit);
                        }
                    }
                });
            }
        };
    }).directive('content', function() {
        var imageRegPattern = /(https?.+\.(?:jpg|jpeg|png|gif))/ig;
        var youtubeRegPattern = /(https?.+\.(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11}))/i;
        var linksRegPattern = /(?:^|\s+)(https?:\/\/?.*[a-zA-Z0-9#$%=@!{},`~&*()'<>?.:;_]*)/ig;

        // I wire the $scope to the DOM.
        function testinput(re, str) {
            return str.search(re) !== -1;
        }

        function link($scope, element) {
                $scope.$watch(
                    '$index',
                    function() {
                        var container = element;
                        var message = $(container).text();
                        var newMessage = '';
                        // Check if message is link to image
                        if (testinput(imageRegPattern, message)) {
                            newMessage = message.replace(imageRegPattern, '<img class="fancy" src = "$1" width = "250"/>');
                            element.html(newMessage);
                            $('img.fancy').on('click', function() {
                                $.fancybox({
                                    href: $(this).attr('src')
                                });
                            });
                        } else if (testinput(youtubeRegPattern, message)) {
                            newMessage = message.replace(youtubeRegPattern, '<iframe width="640" height="360" src="//www.youtube.com/embed/$2" frameborder="0" allowfullscreen></iframe>');

                            element.html(newMessage);
                        } else if (testinput(linksRegPattern, message)) {
                            newMessage = message.replace(linksRegPattern, '<a href="$1" target="_blank">$1</a>');
                            element.html(newMessage);
                        }
                    }
                );

            }
            // Return directive configuration.
        return ({
            link: link,
            restrict: 'A'
        });
    }).factory('focus', function($timeout, $window) {
        return function(id) {
            // timeout makes sure that is invoked after any other event has been triggered.
            // e.g. click events that need to run before the focus or
            // inputs elements that are in a disabled state but are enabled when those events
            // are triggered.
            $timeout(function() {
                var element = $window.document.getElementById(id);
                if (element) {
                    element.focus();
                    var s = element.value;
                    element.value = '';
                    element.value = s;
                }
            });
        };
    }).directive('eventFocus', function(focus) {
        return function(scope, elem, attr) {
            elem.on(attr.eventFocus, function() {
                focus(attr.eventFocusId);
            });

            // Removes bound events in the element itself
            // when the scope is destroyed
            scope.$on('$destroy', function() {
                element.off(attr.eventFocus);
            });
        };
    });
