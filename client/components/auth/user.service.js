'use strict';

angular.module('teamchatApp')
  .factory('User', function($resource) {
    return $resource('/api/users/:action/:controller/:id', {
      // id: '@_id'
    }, {
      changePassword: {
        method: 'PUT',
        params: {
          controller: 'password'
        }
      },
      get: {
        method: 'GET',
        params: {
          id: 'me'
        }
      },
      onlineUsers: {
        method: 'GET',
        params: {
          action: 'onlineUsers',
        },
        isArray: true
      },
      setOnline: {
        method: 'POST',
        params: {
          action: 'online'
        }
      },
      confirm: {
        method: 'GET',
        params: {
          action: 'confirm'
        }
      },
    });
  });
