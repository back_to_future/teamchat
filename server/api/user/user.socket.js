/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var User = require('./user.model');

exports.register = function(socket) {
  User.schema.post('save', function(doc) {
    if (doc.isOnline === 1) {
      socket.emit('user:online', doc);
    } else {
      socket.emit('user:offline', doc);
    }
  });
}
