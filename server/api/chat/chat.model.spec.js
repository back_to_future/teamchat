'use strict';

var should = require('should');
var app = require('../../app');
var User = require('../user/user.model');
var Message = require('./chat.model');

var user = new User({
  provider: 'local',
  name: 'Fake User',
  email: 'test@test.com',
  password: 'password'
});

var message = new Message({
  provider: 'local',
  _user: user,
  content: 'test content',
  notforall: false
});

describe('Message Model', function() {
  beforeEach(function(done) {
    // Clear users before testing
    Message.remove().exec().then(function() {
      done();
    });
  });

  afterEach(function(done) {
    Message.remove().exec().then(function() {
      done();
    });
  });

  it('should begin with no messages', function(done) {
    Message.find({}, function(err, messages) {
      messages.should.have.length(0);
      done();
    });
  });

  it('should be one message after insert', function(done) {
    message.save(function(err) {
      Message.find({}, function(err, messages) {
        messages.should.have.length(1);
        done();
      });
    });
  });

  it('should fail when saving without a content', function(done) {
    message.content = '';
    message.save(function(err) {
      should.exist(err);
      done();
    });
  });

  it('should fail when saving without a user', function(done) {
    message.user = null;
    message.save(function(err) {
      should.exist(err);
      done();
    });
  });
});
