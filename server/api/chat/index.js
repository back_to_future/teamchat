'use strict';

var express = require('express');
var controller = require('./chat.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.index);
router.get('/getHistory', auth.isAuthenticated(), controller.getHistory);

router.post('/clean', auth.hasRole('admin'), controller.clean)
router.post('/getEarliedHistory', auth.isAuthenticated(), controller.getEarliedHistory);
router.post('/addMessage', auth.isAuthenticated(), controller.addMessage);

router.delete('/:id', auth.hasRole('admin'), controller.destroy);


module.exports = router;
