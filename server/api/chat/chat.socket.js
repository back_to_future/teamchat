/**
 * Broadcast updates to client when the model changes
 */

 'use strict';

 var message = require('./chat.model');
 var User = require('../user/user.model');

 exports.register = function(socket) {
 	message.schema.post('save', function (doc) {
 		User.populate(doc, '_user', function(err, combined) {
 			onSave(socket, combined);
 		});
 	});
 }

 function onSave(socket, doc, cb) {
 	socket.emit('message:save', doc);
 }