'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Message Schema
 */

var MessageSchema = new Schema({
  _user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: '{PATH} is required!'
  },
  date: {
    type: Date,
    default: Date.now
  },
  content: {
    type: String,
    required: '{PATH} is required!'
  },
  notforall: {
    type: Boolean,
    default: false
  }

});

module.exports = mongoose.model('Message', MessageSchema);
