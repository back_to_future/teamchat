'use strict';

var Message = require('./chat.model');

var countMessages = 20;

// Get list messages
exports.index = function(req, res) {
  Message.find().populate('_user', 'name').exec(function(err, messsages) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, messsages);
  });
};

/**
 * User add a new message
 */
exports.addMessage = function(req, res) {
  // Create main message object
  var message = new Message(req.body);
  message._user = req.user._id;
  message.content = req.body.message;
  message.notforall = req.body.notforall;
  message.save();
  res.json(message.date || null);
};


function handleError(res, err) {
  return res.send(500, err);
}

// Get last 50 messages
exports.getHistory = function(req, res) {
  Message.find().sort('-date').limit(countMessages).populate('_user', 'name').exec(function(err, message) {
    if (err) {
      return handleError(res, err);
    }
    res.json(200, message);
  });
}

// Get history
exports.getEarliedHistory = function(req, res) {
  var count = countMessages * req.body.offset;
  Message.find().sort('-date').limit(count).populate('_user', 'name').exec(function(err, message) {
    if (err) {
      return handleError(res, err);
    }
    res.json(200, message);
  });
}

exports.clean = function(req, res) {
  Message.remove({}, function(err, message) {
    if (err) {
      return handleError(res, err);
    }
    console.log('collection removed');
    res.json(200);
  });
}

exports.destroy = function(req, res) {
  Message.findByIdAndRemove(req.params.id, function(err, message) {
    if (err) return res.send(500, err);
    return res.send(204);
  });
};
